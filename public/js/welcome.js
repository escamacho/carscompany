function onlyLetters(e) {
    key = e.keyCode || e.which;
    keyboard_key = String.fromCharCode(key).toLowerCase();
    letters = " Ã¡Ã©Ã­Ã³ÃºabcdefghijklmnÃ±opqrstuvwxyz";
    specials = [8, 37, 39, 46];

    keyboard_key_special = false
    for(var i in specials) {
        if(key == specials[i]) {
            keyboard_key_special = true;
            break;
        }
    }

    if(letters.indexOf(keyboard_key) == -1 && !keyboard_key_special)
        return false;
}

function onlyNumbers(e){
    var key = e.charCode;
    return key >= 48 && key <= 57;
}

function loadCity(departmentId){

    const citySelect = document.querySelector('#city');
    $("#city").empty();
    var option = document.createElement('option');
    option.text = 'Seleccionar';
    option.disabled= true;
    option.selected = true;
    citySelect.appendChild(option);


    fetch( "city/" + departmentId)
    .then(response => response.json())
    .then(cities => {
        cities.forEach(city => {

            var cityId = city.id;
            var cityName = city.name;

            var option = document.createElement('option');
            option.value = cityId;
            option.text = cityName;
            
            citySelect.appendChild(option);

        });
    });
}

(function(){
    const departmetnt = document.querySelector('#departament');

    departmetnt.addEventListener('change', (event) => {
      const departmentId = event.target.value;
      loadCity(departmentId);
    });
})();