<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [App\Http\Controllers\WelcomeController::class, 'index'])->name('welcome.index');

Route::get('city/{department}', [App\Http\Controllers\CityController::class, 'getCities'])->name('city.index');
Route::post('competitor/create', [App\Http\Controllers\CompetitorController::class, 'store'])->name('competitor.add');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/logout',  [App\Http\Controllers\Auth\LoginController::class, 'logout']);
