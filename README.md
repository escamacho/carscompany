# CarsCompany

Sitio promocional para recolección de datos de clientes actuales de la marca en Bogotá.
El concurso consiste en que el usuario se registre en una Landing page.
Posteriormente luego de que existan mínimo 5 usuarios seleccione un ganador al azar y lo muestre en la Landing page.

Adicionalmente se puede decargar un archivo excel con los datos de los participantes tan solo con ingresar a la página con
usuario y contraseña, en este caso se ha creado un usuario por defecto (User=> eva@admin.com, Password => admin). 

### Comenzando

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas.


### Pre-requisitos 📋

_Que cosas necesitas para instalar el software y como instalarlas_

```
	wampserver64 o Xampp con php y mysql
```



### Instalación

_Sigue en orden los siguientes pasos:_

_1. Clonar el repositorio_

```
git clone https://escamacho@bitbucket.org/escamacho/carscompany.git
```
_2. Crea la base de base de datos en MySql llamada car-company_

_3. Crea el archivo .env (En el .env.example te dejo lo que debes copiar y pegar)_

_4. Ejecutar los siguientes comandos de composer:_

```
composer install
```

```
composer update
```

_5. Ejecutar los siguientes comandos en orden para las migraciones y seeders:_

```
php artisan migrate
```

_Creamos el usuario por defecto (User=> eva@admin.com, Password => admin) mediante la ejecución del siguiente seeder:_
```
php artisan db:seed --class=UserSeeder
```
_Alimentamos la tabla departamentos mediante la ejecución del siguiente seeder_
```
php artisan db:seed --class=DepartmentSeeder
```

_Alimentamos la tabla ciudades mediante la ejecución del siguiente seeder_

```
php artisan db:seed --class=CitySeeder
```

_6. Corremos el proyecto_
```
php artisan serve
```
