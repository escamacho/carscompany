@extends('layouts.app')
@push('styles')
    <link rel="stylesheet" href="{{ asset('css/welcome.css') }}">
@endpush

@section('content')
    <section class="main-about-us-container">
        <div class="image-header"></div>
          <div class="text-header">
              <h2>¡Extrena Todo Terreno Sin Poner Un Solo Peso!</h2>
              <p>Participa en nuestro sorteo de Navidad y empieza el 2022 con la Nueva BCD todo terreno</p>
              <span>* Aplican TyC</span>
            </div>
          <button class="btn take-part">¡Información del Premio!</button>
    </section>

    @if (isset($winner))
        <section class="main-winner-information">
            <div class="row justify-content-center align-items-center">
                <div class="col-6 text-center">
                    <h1>¡Sorteo cerrado, ya tenemos un Ganador!</h1>
                    <br>
                    <h2>{{$winner->first_name ." " .$winner->last_name}}</h2>
                    <h5>{{$winner->city->name}}</h5>
                </div>

                <div class="col-4">
                    <div class="winner-image"></div>
                </div>
            </div>
        </section>
    @else
    <section class="main-form-register">

        <h5>
            Para participar en el sorteo de la BCD Todo Terreno debes ser registrar tus datos, luego de 5 
            participantes el sistema escogerá un ganador al azar y será informado por medio de correo
            electronico, por favor ingrese los campos con sus datos actualizados.
        </h5>

        <div class="row justify-content-center">
            <div class="col-7">
                @include('layouts.flash_messages')
            </div>
        </div>
        <br>
        <div class="row justify-content-center">
            <div class="col-7 card shadow">
                <form method="post" action="{{ route('competitor.add') }}">
                    @csrf
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="">Nombres</label>
                                <input type="text" class="form-control" id="first_name" name="first_name" onkeypress="return onlyLetters(event)" required>
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="form-group">
                                <label for="">Apellidos</label>
                                <input type="text" class="form-control" id="last_name" name="last_name" onkeypress="return onlyLetters(event)"  required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="">Tipo de documento</label>
                                <select name="document_type" id="document_type" class="form-control" required>
                                    <option selected disabled>Selecionar</option>
                                    <option value="CC">Cedula de Ciudadanía</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="form-group">
                                <label for="">N° de documento</label>
                                <input type="number" class="form-control" id="document_number" name="document_number" onkeypress="return onlyNumbers(event)" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="">Departamento</label>
                                <select name="departament" id="departament" class="form-control" required>
                                    <option></option>
                                    @foreach($departments as $department)
                                        <option value="{{$department->id}}">{{$department->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="form-group">
                                <label for="">Ciudad</label>
                                <select name="city" id="city" class="form-control" required>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="">Celular</label>
                                <input type="number" class="form-control" id="phone_number" name="phone_number" onkeypress="return onlyNumbers(event)"  required>
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="form-group">
                                <label for="">Correo Electronico</label>
                                <input type="email" class="form-control" id="email" name="email">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-8">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox"  id="habeas_data" name="habeas_data" required>
                                <label class="form-check-label" for="habeas_data">
                                    &nbsp;Autorizo el tratamiento de mis datos de acuerdo con la
                                    finalidad establecida en la política de protección de datos personales.
                                </label>
                            </div>
                        </div>
                        <div class="col-4">
                            <button class="btn btn-register">Registrar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>

    @endif
   
    <section class="car-information">
        <div class="row align-items-center">
            <div class="col-8">
                <p>
                    La nueva BCD todo terreno  se encuentra equipada con un motor AMG V8 Biturbo de 4 litros, 
                    que entrega una potencia de 577 hp con una capacidad máxima de torque de 850 Nm.
               </p>
               <p>
                    El G 63 se adaptará a cualquier tipo de superficie, ya sea ciudad, carretera o a campo traviesa, 
                    de acuerdo a tus preferencias. El AMG RIDE CONTROL te entrega la posibilidad de alterar los trenes 
                    de rodaje a tu gusto. Tan solo apretando un botón, podrás elegir entre cuatro tipos de conducción: 
                    Confort, Sport, Eco o Individual.
               </p>
                <p>
                    El modelo G puede adecuar su tracción para obtener una mejor performance dependiendo del terreno 
                    en que se encuentre. Estos pueden activarse manualmente para poder afrontar las situaciones más 
                    extremas, logrando transmitir la fuerza de propulsión necesaria para garantizar el avance del vehículo,
                     así como su tracción.
                </p>
        
                <p>
                    Se recomienda accionar estos bloqueos solo al conducir sobre terrenos no compactados, debido a los efectos
                    que podría tener en la dirección y seguridad, pues se desactivarán los sistemas de regulación tales como
                    ESP(R) y ABS, así como los sistemas de asistencia a la conducción.
                </p>
                <p>
                    El modelo G puede adecuar su tracción para obtener una mejor performance dependiendo del terreno 
                    en que se encuentre. Estos pueden activarse manualmente para poder afrontar las situaciones más 
                    extremas, logrando transmitir la fuerza de propulsión necesaria para garantizar el avance del vehículo,
                     así como su tracción.
                </p>
        
                <p>
                    Se recomienda accionar estos bloqueos solo al conducir sobre terrenos no compactados, debido a los efectos
                    que podría tener en la dirección y seguridad, pues se desactivarán los sistemas de regulación tales como
                    ESP(R) y ABS, así como los sistemas de asistencia a la conducción.
                </p>
                <p>
                    El modelo G puede adecuar su tracción para obtener una mejor performance dependiendo del terreno 
                    en que se encuentre. Estos pueden activarse manualmente para poder afrontar las situaciones más 
                    extremas, logrando transmitir la fuerza de propulsión necesaria para garantizar el avance del vehículo,
                     así como su tracción.
                </p>
        
                <p>
                    Se recomienda accionar estos bloqueos solo al conducir sobre terrenos no compactados, debido a los efectos
                    que podría tener en la dirección y seguridad, pues se desactivarán los sistemas de regulación tales como
                    ESP(R) y ABS, así como los sistemas de asistencia a la conducción.
                </p>
                
            </div>

            <div class="col-4">
                <div class="promotional-image"></div>
            </div>
        </div>

    </section>

    <section class="take-part-information">
        <hr>
        <div class="row align-items-center">
            <div class="col-4">
                <div class="car-internal-image"></div>
            </div>
            <div class="col-8">
                <p>
                    De igual modo, este clásico todoterreno de Mercedes, integra un sistema activo de alerta de cambio de 
                    carril, que da asistencia a la conducción cuando detecta un cambio innecesario o peligroso hacia otra 
                    pista, advirtiendo al conductor mediante vibraciones del volante e interviniendo los frenos del 
                    vehículo, con el fin de devolverlo a su carril. Este sensor es capaz de funcionar cuando se conduce 
                    a velocidades entre 60 y 200 km/h.
                </p>
                <p>
                    Destaca además su moderno sistema Attention Assist de alerta por cansancio, que por medio de 
                    sensores, permite al automóvil detectar si el conductor necesita tomar un descanso, evitando 
                    correr el riesgo de que ocurra un accidente.
                </p>
        
                <p>
                    De igual modo, este clásico todoterreno de Mercedes, integra un sistema activo de alerta de cambio de 
                    carril, que da asistencia a la conducción cuando detecta un cambio innecesario o peligroso hacia otra 
                    pista, advirtiendo al conductor mediante vibraciones del volante e interviniendo los frenos del 
                    vehículo, con el fin de devolverlo a su carril. Este sensor es capaz de funcionar cuando se conduce 
                    a velocidades entre 60 y 200 km/h.
                </p>
        
                <p>
                    Destaca además su moderno sistema Attention Assist de alerta por cansancio, que por medio de 
                    sensores, permite al automóvil detectar si el conductor necesita tomar un descanso, evitando 
                    correr el riesgo de que ocurra un accidente.
                </p>
            </div>
        </div>
        <hr>
    </section>

@endsection
@section('scripts')
    <script src="{{ asset('js/welcome.js') }}"></script>
@stop