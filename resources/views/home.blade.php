@extends('layouts.app')
@push('styles')
    <link rel="stylesheet" href="{{ asset('css/home.css') }}">
@endpush

@section('content')
<div class="container">
    <div class="row justify-content-center table-container">
        <div class="col-12">
            <table  id="example" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th width="15%">Competidor</th>
                        <th width="10%">Tipo de documento</th>
                        <th width="10%">Número de documento</th>
                        <th width="10%" >Departamento</th>
                        <th width="10%" >Ciudad</th>
                        <th width="10%" >Celular</th>
                        <th width="10%">Correo</th>
                        <th width="15%">Fecha de Registro</th>
                        <th width="10%">Estado</th>
                    </tr>
                </thead>
                <tbody>
                   @foreach($competitors as $competitor)
                   <tr>
                       <td>{{$competitor->full_name}}</td>
                       <td>{{$competitor->document_type}}</td>
                       <td>{{$competitor->document_number}}</td>
                       <td>{{$competitor->city->department->name}}</td>
                       <td>{{$competitor->city->name}}</td>
                       <td>{{$competitor->phone}}</td>
                       <td>{{$competitor->email}}</td>
                       <td>{{$competitor->created_at}}</td>
                       <td>{{$competitor->status}}</td>
                   </tr>
                   @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script>
    $(document).ready(function() {
        $('#example').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'excel',
                    title: 'Reporte de Competidores',            
                }
            ]
        } );
    } );
    </script>
@stop
