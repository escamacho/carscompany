<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Competitor;
use App\Models\Department;

class WelcomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       //consultamos si hay ganadores
       $winner = Competitor::where('status','Winner')->first();

       //consultamos los departamentos

       $departments = Department::orderBy('name', 'ASC')->get();

       if($winner){
            return view('welcome',compact('winner'));
       }else{
            return view('welcome', compact('departments'));
       }

       
    }
}
