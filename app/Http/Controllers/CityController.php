<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\City;
use App\Models\Department;

class CityController extends Controller
{

    public function getCities(Department $department)
    {
     
       $cities = City::where('department_id',$department->id)->get();
       return $cities;

    }
}
