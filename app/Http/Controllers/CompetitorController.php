<?php

namespace App\Http\Controllers;

use App\Models\Competitor;
use Illuminate\Http\Request;

class CompetitorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'first_name' => 'required|min:3',
            'last_name' => 'required|min:3',
            'document_type' => 'required',
            'document_number' => 'numeric|min:8|required|unique:competitors',
            'city' => 'required',
            'phone_number' => 'numeric|required|min:10',
            'email' => 'unique:competitors'
        ];
        $request->validate($rules);
        
        Competitor::create([
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'document_type' => $request->input('document_type'),
            'document_number' => $request->input('document_number'),
            'city_id' => $request->input('city'),
            'phone' => $request->input('phone_number'),
            'email' => $request->input('email'),
        ]);

        //Verificamos si hay ganadores

        $competitors = Competitor::all();

        if($competitors->count()===5){
            // Buscamos uno al azar
            $competitor = Competitor::orderByRaw("RAND()")->limit(1)->first();
            $competitor->status = 'Winner';
            $competitor->save();
        }

        return back()->with([
            'flash_message' => '¡Registro exitoso!',
            'flash_message_important' => true,
            'flash_alert_type' => "success",
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Competitor  $competitor
     * @return \Illuminate\Http\Response
     */
    public function show(Competitor $competitor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Competitor  $competitor
     * @return \Illuminate\Http\Response
     */
    public function edit(Competitor $competitor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Competitor  $competitor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Competitor $competitor)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Competitor  $competitor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Competitor $competitor)
    {
        //
    }
}
