<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Department;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $departments = array(
            array(                 
                'id' => 1,
                'name' => 'Amazonas',
            ),
            array(                 
                'id' => 2,
                'name' => 'Antioquia',
            ),
            array(                 
                'id' => 3,
                'name' => 'Arauca',
            ),
            array(                 
                'id' => 4,
                'name' => 'Atlántico',
            ),
            array(                 
                'id' => 5,
                'name' => 'Bolívar',
            ),
            array(                 
                'id' => 6,
                'name' => 'Boyacá',
            ),
            array(                 
                'id' => 7,
                'name' => 'Caldas',
            ),
            array(                 
                'id' => 8,
                'name' => 'Caquetá',
            ),
            array(                 
                'id' => 9,
                'name' => 'Casanare',
            ),
            array(                 
                'id' => 10,
                'name' => 'Cauca',
            ),
            array( 
                'id' => 11,
                'name' => 'Cesar',
            ),
            array( 
                'id' => 12,
                'name' => 'Chocó',
            ),
            array( 
                'id' => 13,
                'name' => 'Córdoba',
            ),
            array( 
                'id' => 14,
                'name' => 'Cundinamarca',
            ),
            array( 
                'id' => 15,
                'name' => 'Guainia',
            ),
            array( 
                'id' => 16,
                'name' => 'Guaviare',
            ),
            array( 
                'id' => 17,
                'name' => 'Huila',
            ),
            array( 
                'id' => 18,
                'name' => 'La Guajira',
            ),
            array( 
                'id' => 19,
                'name' => 'Magdalena',
            ),
            array( 
                'id' => 20,
                'name' => 'Meta',
            ),
            array( 
                'id' => 21,
                'name' => 'Nariño',
            ),
            array( 
                'id' => 22,
                'name' => 'Norte de Santander',
            ),
            array( 
                'id' => 23,
                'name' => 'Putumayo',
            ),
            array( 
                'id' => 24,
                'name' => 'Quindio',
            ),
            array( 
                'id' => 25,
                'name' => 'Risaralda',
            ),
            array( 
                'id' => 26,
                'name' => 'San Andr�s y Providencia',
            ),
            array( 
                'id' => 27,
                'name' => 'Santander',
            ),
            array( 
                'id' => 28,
                'name' => 'Sucre',
            ),
            array( 
                'id' => 29,
                'name' => 'Tolima',
            ),
            array( 
                'id' => 30,
                'name' => 'Valle del Cauca',
            ),
            array( 
                'id' => 31,
                'name' => 'Vaupés',
            ),
            array( 
                'id' => 32,
                'name' => 'Vichada',
            ),
        );

        foreach($departments as $department){
            $dep = new Department();
            $dep->name = $department['name'];
            $dep->save();

        }
    
    }
}
